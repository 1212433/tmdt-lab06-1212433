﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TMDT_Lab06_1212433.Models;

namespace TMDT_Lab06_1212433.Controllers
{
    public class usersController : Controller
    {
        public string mssv = "";
        public List<DetailsModels> lst = new List<DetailsModels>();
        //
        // GET: /users/
        [Authorize]
        public ActionResult Index()
        {
            LoadSessionObject();
            return View();
        }

        private void CreateDB1212433()
        {
            DetailsModels sv;
            sv = new DetailsModels("1212000", "Nguyễn Văn A", "10/10/1994");
            lst.Add(sv);
            sv = new DetailsModels("1212001", "Nguyễn Thị B", "11/10/1994");
            lst.Add(sv);
            sv = new DetailsModels("1212002", "Lê Văn C", "10/12/1994");
            lst.Add(sv);
        }

        /// <summary>
        /// ActionResult for ordinary session(HttpContext).
        /// </summary>
        /// <param name="sessionValue"></param>
        /// <returns></returns>
        public ActionResult SaveSession(string sessionValue)
        {
            try
            {
                CreateDB1212433();
                for (int i = 0; i < lst.Count; i++ )
                {
                    if (lst[i]._mssv == sessionValue)
                    {
                        System.Web.HttpContext.Current.Session["mssv"] = lst[i]._mssv;
                        System.Web.HttpContext.Current.Session["name"] = lst[i]._name;
                        System.Web.HttpContext.Current.Session["bday"] = lst[i]._bday;
                        mssv = lst[i]._mssv;
                    }
                }

                if (mssv == "")
                    System.Web.HttpContext.Current.Session["error"] = "Không tồn tại mssv này.";

                return RedirectToAction("Index");
            }
            catch (InvalidOperationException)
            {
                return View("Index");
            }
        }

        /// <summary>
        /// Store the session value to ViewData.
        /// </summary>
        private void LoadSessionObject()
        {
            // Load session from HttpContext.
            ViewData["mssv"] = System.Web.HttpContext.Current.Session["mssv"] as String;
            ViewData["name"] = System.Web.HttpContext.Current.Session["name"] as String;
            ViewData["bday"] = System.Web.HttpContext.Current.Session["bday"] as String;
            ViewData["error"] = System.Web.HttpContext.Current.Session["error"] as String;
        }
    }
}
