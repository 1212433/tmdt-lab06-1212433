﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Security;
using TMDT_Lab06_1212433.Models;

namespace TMDT_Lab06_1212433.Controllers
{
    public class authController : Controller
    {
        //
        // GET: /auth/
        public ActionResult login()
        {
            return View();
        }

        [HttpPost]
        public ActionResult login(LoginModels model, string returnUrl)
        {
            // Lets first check if the Model is valid or not
            if (ModelState.IsValid)
            {
                string username = model.Username;
                string password = model.Password;
                bool userValid = false;

                if (username == "1212433" && password == "TMDT2015")
                    userValid = true;

                // User found in the database
                if (userValid)
                {

                    FormsAuthentication.SetAuthCookie(username, false);
                    if (Url.IsLocalUrl(returnUrl) && returnUrl.Length > 1 && returnUrl.StartsWith("/")
                        && !returnUrl.StartsWith("//") && !returnUrl.StartsWith("/\\"))
                    {
                        return Redirect(returnUrl);
                    }
                    else
                    {
                        return RedirectToAction("Index", "users");
                    }
                }
                else
                {
                    ModelState.AddModelError("", "Username hoặc Password không đúng.");
                }
            }

            // If we got this far, something failed, redisplay form
            return View(model);
        }

        public ActionResult logout()
        {
            FormsAuthentication.SignOut();

            return RedirectToAction("Index", "users");
        }
    }
}
