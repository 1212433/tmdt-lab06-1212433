﻿using System.Web;
using System.Web.Mvc;

namespace TMDT_Lab06_1212433
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
        }
    }
}