﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace TMDT_Lab06_1212433.Models
{
    public class DetailsModels
    {
        public string _mssv { get; set; }
        public string _name { get; set; }
        public string _bday { get; set; }

        public DetailsModels(string mssv, string name, string bday)
        {
            _mssv = mssv;
            _name = name;
            _bday = bday;
        }
    }
}